#!/bin/bash

INSTALLERVERSION="1.0.0"

function greenMessage {
  echo -e "\\033[32;1m${*}\033[0m"
}

function magentaMessage {
  echo -e "\\033[35;1m${*}\033[0m"
}

function cyanMessage {
  echo -e "\\033[36;1m${*}\033[0m"
}

function redMessage {
  echo -e "\\033[31;1m${*}\033[0m"
}

function yellowMessage {
  echo -e "\\033[33;1m${*}\033[0m"
}

function errorQuit {
  errorExit 'Exit now!'
}

function errorExit {
  redMessage "${@}"
  exit 1
}

function errorContinue {
  redMessage "Invalid option."
  return
}

function pre_initialization {

	if [ "$(id -u)" != "0" ]; then
	  cyanMessage "Change to root account required"
	  su root
	fi

	if [ "$(id -u)" != "0" ]; then
	  errorExit "Still not root, aborting"
	fi
	
  echo "deb http://ftp.de.debian.org/debian wheezy-backports main" >> /etc/apt/sources.list >/dev/null 2>&1
  apt-get update >/dev/null 2>&1
  apt-get -qq install wget ca-certificates unzip shc -y >/dev/null 2>&1

}

function checkOS {

	if [ -f /etc/debian_version ] || [ -f /etc/centos-release ]; then
	  return 0
	else
	  return 1
	fi

}

function pre_initialization {

	if [ "$(id -u)" != "0" ]; then
	  cyanMessage "Change to root account required"
	  su root
	fi

	if [ "$(id -u)" != "0" ]; then
	  errorExit "Still not root, aborting"
	fi

}

function install_dep {

  echo "deb http://ftp.de.debian.org/debian wheezy-backports main" > /etc/apt/sources.list >/dev/null 2>&1
  apt-get update >/dev/null 2>&1
  apt-get -qq install wget ca-certificates unzip shc -y >/dev/null 2>&1

}

function initial {

  cyanMessage "Running pre initialization..."
  pre_initialization

	if [ checkOS ] ; then
	  greenMessage "OS supported. Installer goes on."
	else
	  redMessage "OS not supported. The installer only supports Debian, Ubuntu and CentOS. Aborting..."
	  exit 1
	fi

  cyanMessage "Installing nessesary dependencies... This may take up to 5 minutes..."
  
  install_dep

  cyanMessage "Downloading CloudNet installer..."
  
  rm /tmp/cloudnet_installer.sh >/dev/null 2>&1
  wget -q -O /tmp/cloudnet_installer.sh https://gitlab.com/MiniMinerLPs/CloudNetInstaller/raw/master/cloudnet_installer.sh.x >/dev/null 2>&1

  cyanMessage "Step 1 / 5 complete!"

  chmod 744 /tmp/cloudnet_installer.sh >/dev/null 2>&1
  
  cyanMessage "Step 2 / 5 complete!"

  echo ""
  greenMessage "Download successful! Installer will start in 5 seconds..."
  sleep 5
  bash /tmp/cloudnet_installer.sh

}

initial